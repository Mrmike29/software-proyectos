import { Component } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {

  public titulo: string;
  public comentario: string;
  public year: number;

  constructor() {
    this.titulo = 'Log In';
    this.comentario = 'Ya estoy entrando a las grandes ligas mamá';
    this.year = 2020;
    console.log('mi primer componente cargado');
  }
}
// import { Component, OnInit } from '@angular/core';
//
// @Component({
//   selector: 'app-login',
//   templateUrl: './login.component.html',
//   styleUrls: ['./login.component.css']
// })
// export class LoginComponent implements OnInit {
//
//   constructor() { }
//
//   ngOnInit(): void {
//   }
//
// }
